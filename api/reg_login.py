import app


class RegLoginApi:

    def __init__(self, session):
        self.session = session

    # 获取图片验证码接口
    def get_img_verify_code(self, r):
        url = app.Base_host + "/common/public/verifycode1/{}".format(r)
        return self.session.get(url=url)

    # 获取短信验证码接口
    def get_phone_verify_code(self, phone, imgVerifyCode, type="reg"):
        url = app.Base_host + "/member/public/sendSms"
        # 定义一个字典变量，用来接受请求体数据
        form_dict = {
            "phone": phone,
            "imgVerifyCode": imgVerifyCode,
            "type": type
        }
        return self.session.post(url=url, data=form_dict)

    # 注册接口
    def user_register(self, phone, password, verifycode, phone_code, dy_server, invite_phone=None):
        url = app.Base_host + "/member/public/reg"
        form_dict = {"phone": phone, "password": password, "verifycode": verifycode,
                     "phone_code": phone_code, "dy_server": dy_server, "invite_phone": invite_phone}
        return self.session.post(url=url, data=form_dict)

    # 登录接口
    def user_login(self, keywords, password):
        url = app.Base_host + "/member/public/login"
        form_dict = {"keywords": keywords, "password": password}
        return self.session.post(url=url, data=form_dict)
