import app


class RechargeApi:
    def __init__(self, session):
        self.session = session

    # 获取充值验证码接口

    def get_cz_verify_code(self, r):
        url = app.Base_host + "/common/public/verifycode/{}".format(r)
        return self.session.get(url=url)

    # 获取充值成功接口
    def recharge_success(self,amount,valicode):
        url = app.Base_host + '/trust/trust/recharge'
        form_dict = {"paymentType": "chinapnrTrust", "amount": amount, "formStr": "reForm", "valicode": valicode}

        return self.session.post(url=url,data=form_dict)
    # 获取第三方充值成功接口
    def third_success(self, url, dict_data):
        return self.session.post(url=url,data=dict_data)
