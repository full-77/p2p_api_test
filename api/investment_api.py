import app


class InvestmentApi:
    def __init__(self, session):
        self.session = session

    # 获取投资成功接口
    def investment_success(self, id, amount):
        url = app.Base_host + "/trust/trust/tender"
        # 定义一个字典变量，用来接受请求体数据
        form_dict = {"id": id, "depositCertificate": -1, "amount": amount}
        return self.session.post(url=url, data=form_dict)

    # 获取第三方投资成功接口
    def third_investment(self, url, form_data):
        return self.session.post(url=url, data=form_data)
