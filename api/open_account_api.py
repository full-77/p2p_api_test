import app


class OpenAcountApi:
    def __init__(self,session):
        self.session = session

    # 实名认证接口
    def realname(self,realname,card_id):
        url = app.Base_host + "/member/realname/approverealname"
        # 定义一个变量接受请求体数据
        form_dict = {"realname":realname,"card_id":card_id}
        return self.session.post(url=url,data=form_dict,files={'x':'y'})

    # 开户接口
    def open_account(self):
        url = app.Base_host + "/trust/trust/register"
        return self.session.post(url=url)

    # 第三方开户
    def third_open_account(self,url,data_dict):
        return self.session.post(url=url,data=data_dict)

