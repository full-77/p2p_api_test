import unittest
from time import sleep

import requests
from parameterized import parameterized

import utils
from api.reg_login import RegLoginApi


class TestRegLogin(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # 用户信息初始化
        utils.clear_user_info()
    def setUp(self) -> None:
        # 创建session对象
        self.session = requests.session()
        # 实例化封装登录注册类的对象
        self.reg_login_api = RegLoginApi(self.session)

    def tearDown(self) -> None:
        # 关闭session对象
        self.session.close()

    # 获取图片验证码测试用例
    @parameterized.expand(utils.red_json("reg_login_case_data", "img_verify_code"))
    def test01_img_verify_code(self,r,exp_status_code):

        # 调用获取图片验证码封装方法，获取接口响应结果
        res = self.reg_login_api.get_img_verify_code(r)
        # 获取实际接口返回的状态码
        act_status_code = res.status_code
        # 调用unittest提供的断言方法，对结果进行校验
        self.assertEqual(exp_status_code, act_status_code)

    # 获取短信验证码测试用例
    @parameterized.expand(utils.red_json("reg_login_case_data", "phone_verify_code"))
    def test02_phone_verify_code(self,phone,imgVerifyCode,exp_status_code,exp_status,description):
        # 调用图片验证码方法
        self.reg_login_api.get_img_verify_code(0.12345666)
        # 调用获取短信验证码方法
        res = self.reg_login_api.get_phone_verify_code(phone, imgVerifyCode)
        # 调用公共断言方法
        utils.common_assert(self,res,exp_status_code,exp_status,description)
    # 注册测试用例
    @parameterized.expand(utils.red_json("reg_login_case_data", "user_register"))
    def test03_register(self,phone,password,imgVerifyCode,phoneVerifyCode,dyServer,
                        invitePhone,exp_status_code,exp_status,description):
        # 调用图片验证码方法
        self.reg_login_api.get_img_verify_code(0.12345666)
        # 调用获取短信验证码方法
        self.reg_login_api.get_phone_verify_code(phone, "8888")
        # 调用注册封装方法，获取响应结果
        res = self.reg_login_api.user_register(phone, password, imgVerifyCode, phoneVerifyCode, dyServer,invitePhone,)
        # 获取字典类型响应体数据
        print(res.json())
        # 调用公共断言方法
        utils.common_assert(self, res, exp_status_code, exp_status, description)

    # 登录测试用例
    @parameterized.expand(utils.red_json("reg_login_case_data", "user_login"))
    def test04_user_login(self,mobile,password,exp_status_code,exp_status,description):
        # 调用登录接口封装方法，获取接口响应将结果(response)
        res = self.reg_login_api.user_login(mobile, password)
        # 获取字典类型响应体数据
        print(res.json())
        # 调用公共断言方法
        utils.common_assert(self,res,exp_status_code,exp_status,description)
    @unittest.skip
    def test05_user_login_check(self):
        # 调用登录接口封装方法，获取接口响应将结果(response)
        res1 = self.reg_login_api.user_login("17104560011", 'qq23456')
        # 获取字典类型响应体数据
        print(res1.json())
        # 调用公共断言方法
        utils.common_assert(self, res1, 200, 100, '密码错误1次')
        # 调用登录接口封装方法，获取接口响应将结果(response)
        res2 = self.reg_login_api.user_login("17104560011", 'qq23456')
        # 获取字典类型响应体数据
        print(res2.json())
        # 调用公共断言方法
        utils.common_assert(self, res2, 200, 100, '密码错误2次')
        # 调用登录接口封装方法，获取接口响应将结果(response)
        res3 = self.reg_login_api.user_login("17104560011", 'qq23456')
        # 获取字典类型响应体数据
        print(res3.json())
        # 调用公共断言方法
        utils.common_assert(self, res3, 200, 100, '账号已被锁定')
        # 等待60s,登录成功
        sleep(60)
        # 调用登录接口封装方法，获取接口响应将结果(response)
        res = self.reg_login_api.user_login("17104560011", 'qq123456')
        # 获取字典类型响应体数据
        print(res.json())
        # 调用公共断言方法
        utils.common_assert(self, res, 200, 200, '成功')