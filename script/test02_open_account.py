import unittest
import requests
from parameterized import parameterized

import utils
from api.open_account_api import OpenAcountApi
from api.reg_login import RegLoginApi


class TestOpenAccount(unittest.TestCase):
    def setUp(self) -> None:
        # 创建session对象
        self.session = requests.session()
        # 实例化封装接口的类
        self.opend_account_api = OpenAcountApi(self.session)
        self.reg_login_api = RegLoginApi(self.session)

    def tearDown(self) -> None:
        self.session.close()
    # 实名认证测试用例
    @parameterized.expand(utils.red_json('open_account_case_data','realname'))
    def test01_realname(self,mobile,pwd,name,card_id,exp_status,description):
        self.reg_login_api.user_login(mobile, pwd)
        # 调用封装接口，获取实名认证方法
        res = self.opend_account_api.realname(name,card_id)
        # 打印响应数据
        print(res.json())
        # 调用公共断言
        utils.common_assert(self,res,200,exp_status,description)
        # 实名认证信息断言
        if res.json().get('status') == 200:
            self.assertEqual('632****500',res.json().get('data').get('card_id'))
            self.assertEqual('张**',res.json().get('data').get('realname'))
    def test02_open_account(self):
        #调用登录接口，完成用户登录
        self.reg_login_api.user_login('17045600001','qq123456')
        # 调用封装接口，获取开户方法
        res = self.opend_account_api.open_account()
        # 打印响应数据
        print(res.json())
        # 调用公共断言
        utils.common_assert(self, res, 200, 200, 'form')

    def test03_third_open_account(self):
        #调用登录接口，完成用户登录
        self.reg_login_api.user_login('17045600001','qq123456')
        # 完成开户接口，获取response对象
        res = self.opend_account_api.open_account()
        # 打印响应数据
        print(res.json())
        # 调用获取第三方接口请求数据方法，获取第三方开户url
        url = utils.html_util(res)[0]
        # 调用获取第三方接口请求数据方法，获取第三方开户请求体数据
        body_data = utils.html_util(res)[1]
        # 调用获取第三方接口的方法，获取第三方开户的响应结果
        res1 = self.opend_account_api.third_open_account(url,body_data)
        print('响应体数据：',res1.text)
        # 断言
        self.assertEqual(200,res1.status_code)
        # self.assertIn(',res1.text)
