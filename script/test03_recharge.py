import unittest
import requests
from parameterized import parameterized

import utils
from api.recharge_api import RechargeApi
from api.reg_login import RegLoginApi


class TestRecharge(unittest.TestCase):
    def setUp(self) -> None:
        # 创建session对象
        self.session = requests.session()
        # 实例化封装登录注册类的对象--充值
        self.recharge_api = RechargeApi(self.session)
        # 实例化封装登录注册类的对象--登录
        self.reg_login_api = RegLoginApi(self.session)
        # 调用登录接口，完成用户登录
        self.reg_login_api.user_login('17045600001', 'qq123456')
    def tearDown(self) -> None:
        # 关闭session对象
        self.session.close()

    @parameterized.expand(utils.red_json('recharge_case_data', 'recharge_verify_code'))
    def test01_recharge_verify_code(self,r,exp_status_code):
        # 调用获取图片验证码封装方法，获取接口响应结果
        res = self.recharge_api.get_cz_verify_code(r)
        # 调用unittest提供的断言方法，对结果进行校验
        self.assertEqual(exp_status_code, res.status_code)

    @parameterized.expand(utils.red_json('recharge_case_data', 'recharge_success'))
    def test02_recharge_success(self,amount,valicode,exp_status_code,exp_status,description):
       # 调用获取充值验证码接口
        self.recharge_api.get_cz_verify_code(0.15648)
        # 调用获取充值成功封装方法，获取响应结果
        res = self.recharge_api.recharge_success(amount,valicode)
        # 打印响应体数据
        print(res.text)
        # 调用公共断言
        utils.common_assert(self, res, exp_status_code, exp_status, description)
    def test03_third_recharge(self):

        # 调用获取充值验证码接口
        self.recharge_api.get_cz_verify_code(0.15648)
        # 调用获取充值成功封装方法，获取响应结果
        res1 = self.recharge_api.recharge_success("100000", "8888", )
        print(res1.json())
        # 调用获取第三方接口请求数据方法，获取第三方开户url
        url = utils.html_util(res1)[0]
        # 调用获取第三方接口请求数据方法，获取第三方开户请求体数据
        dict_data = utils.html_util(res1)[1]
        # 获取第三方接口返回的响应结果
        res2 = self.recharge_api.third_success(url,dict_data)
        print(res2.text)
        # 断言
        self.assertEqual(200, res2.status_code)
        self.assertIn('OK',res2.text)