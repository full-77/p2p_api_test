import unittest

import requests
from parameterized import parameterized

import utils
from api.investment_api import InvestmentApi
from api.reg_login import RegLoginApi
from utils import init_tender_info


class TestTender(unittest.TestCase):
    @classmethod
    def setUpClass(cls) -> None:
        # 借款标初始化
        init_tender_info()
    def setUp(self) -> None:
        # 创建session对象
        self.session = requests.session()
        # 调用登录模块类
        self.reg_login_api = RegLoginApi(self.session)
        # 调用投资模块类
        self.investmen_api = InvestmentApi(self.session)

    def tearDown(self) -> None:
        # 关闭session对象
        self.session.close()

    @parameterized.expand(utils.red_json('tender_case_data', 'invest_success'))
    def test01_tender(self,mobile,pwd,id,amount,exp_status_code,exp_status,description):
        # 登录成功
        self.reg_login_api.user_login(mobile,pwd)
        # 调用投资成功接口
        res = self.investmen_api.investment_success(id,amount)
        print('请求体数据：',res.json())
        # 调用公共断言
        utils.common_assert(self, res, exp_status_code, exp_status, description)
    def test02_third_tender(self):
        # 登录成功
        self.reg_login_api.user_login('17045600001', 'qq123456')
        # 调用投资成功接口
        res = self.investmen_api.investment_success('642', '1000')
        # 调用获取第三方接口请求数据方法，获取第三方开户url
        url = utils.html_util(res)[0]
        # 调用获取第三方接口请求数据方法，获取第三方开户请求体数据
        dict_data = utils.html_util(res)[1]
        # 获取第三方接口返回的响应结果
        res2 = self.investmen_api.third_investment(url, dict_data)
        print(res2.text)
        # 断言
        self.assertEqual(200, res2.status_code)
        self.assertIn('OK', res2.text)



