import unittest

import requests

import utils
from api.investment_api import InvestmentApi
from api.open_account_api import OpenAcountApi
from api.recharge_api import RechargeApi
from api.reg_login import RegLoginApi
from utils import init_tender_info
class TzFlow(unittest.TestCase):
    def test01(self):
        # 创建session对象
        self.session = requests.session()
        # 实例化封装登录注册类的对象--充值
        self.recharge_api = RechargeApi(self.session)
        # 实例化封装登录注册类的对象--登录
        self.reg_login_api = RegLoginApi(self.session)
        # 实例化封装登录注册类的对象--开户
        self.opend_account_api = OpenAcountApi(self.session)
        # 调用投资模块类
        self.investmen_api = InvestmentApi(self.session)
        # 借款标初始化
        init_tender_info()
        # 调用获取图片验证码封装方法，获取接口响应结果
        self.recharge_api.get_cz_verify_code(0.457891516)
        # 调用获取短信验证码方法
        self.reg_login_api.get_phone_verify_code("17200001122", "8888")
        # 调用注册封装方法，获取响应结果
        self.reg_login_api.user_register("17200001122", "qq123456", "8888", "666666", "on")
        # 调用登录接口封装方法，获取接口响应将结果(response)
        self.reg_login_api.user_login("17200001122", "qq123456")
        # 调用封装接口，获取实名认证方法
        self.opend_account_api.realname("上帝投资", "632323190605260500")
        # 调用封装接口，获取开户方法
        res_kh = self.opend_account_api.open_account()
        # 调用获取第三方接口请求数据方法，获取第三方开户url
        url1 = utils.html_util(res_kh)[0]
        # 调用获取第三方接口请求数据方法，获取第三方开户请求体数据
        body_data = utils.html_util(res_kh)[1]
        # 调用获取第三方接口的方法，获取第三方开户的响应结果
        self.opend_account_api.third_open_account(url1, body_data)
        # 调用获取图片验证码封装方法，获取接口响应结果
        self.recharge_api.get_cz_verify_code(0.2567879)
        # 调用获取充值成功封装方法，获取响应结果
        res_cz_success = self.recharge_api.recharge_success("100000", "8888")
        # 调用获取第三方接口请求数据方法，获取第三方开户url
        url2 = utils.html_util(res_cz_success)[0]
        # 调用获取第三方接口请求数据方法，获取第三方开户请求体数据
        dict_data = utils.html_util(res_cz_success)[1]
        # 获取第三方接口返回的响应结果
        self.recharge_api.third_success(url2, dict_data)
        # 调用投资成功接口
        res3 = self.investmen_api.investment_success('642', '1000')
        # 调用获取第三方接口请求数据方法，获取第三方开户url
        url = utils.html_util(res3)[0]
        # 调用获取第三方接口请求数据方法，获取第三方开户请求体数据
        dict_data1 = utils.html_util(res3)[1]
        # 获取第三方接口返回的响应结果
        res4 = self.investmen_api.third_investment(url, dict_data1)
        # 断言
        self.assertEqual(200,res4.status_code)
        self.assertIn('OK',res4.text)
