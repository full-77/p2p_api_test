# 1、导包
from bs4 import BeautifulSoup

data = """
<html> 
    <head>
        <title>黑马程序员</title>
    </head> 
    <body>
        <p id="test01">软件测试</p>
        <p id="test02">2020年</p>
        <a href="/api.html">接口测试</a>
        <a href="/web.html">Web自动化测试</a> 
        <a href="/app.html">APP自动化测试</a>
</body>
</html>
"""
# 2、获取BeautifulSoup对象
bs = BeautifulSoup(data, "html.parser")

# 3、调用相关方法
# 获取title标签的全部数据
print(bs.title)

# # 获取title标签的内容
print(bs.title.string)

# # 获取p标签的第一条数据，id属性的值
print(bs.p.get('id'))
# 获取所有a标签
print(bs.find_all('a'))
# 获取所有a标签的href属性的值
a_href = bs.find_all('a')   #  a 标签以列表形式获取
href_list = list()
print(a_href)
for x in a_href:
    href_list.append(x['href'])   # 获取href 属性值，添加新列表
print(href_list)


def get_bs(data,a):
    bs = BeautifulSoup(data, "html.parser")
    a_tmp = bs.find_all('a')
    for a_list in a_tmp:

