
import unittest

from unittestreport import TestRunner

import app
from script.test01_reg_login import TestRegLogin
from script.test02_open_account import TestOpenAccount
# 实例化测试套件
from script.test03_recharge import TestRecharge
from script.test04_investment import TestTender
from script.test05_tz import TzFlow

suite = unittest.TestSuite()
# 添加测试类
suite.addTest(unittest.makeSuite(TestRegLogin))
suite.addTest(unittest.makeSuite(TestOpenAccount))
suite.addTest(unittest.makeSuite(TestRecharge))
suite.addTest(unittest.makeSuite(TestTender))
suite.addTest(unittest.makeSuite(TzFlow))
# 定义测试报告存放的目录
report_path = app.Base_path + '/report/'
# 实例化TestRunner
runner = TestRunner(suite,'report_html',report_path,'p2p接口自动化','Jm',"投资业务流程")
# 调用run方法生成测试报告
runner.run()
# # 实例化TextTestRunner类
# runner = unittest.TextTestRunner()
# # 运行测试套件
# runner.run(suite)